# This file has been automatically generated, DO NOT EDIT!

# Frozen requirements for "./compose.in"

backports.ssl-match-hostname==3.5.0.1
cached-property==1.4.0
certifi==2018.1.18
chardet==3.0.4
docker==3.1.4
docker-compose==1.20.1
docker-pycreds==0.2.2
dockerpty==0.4.1
docopt==0.6.2
enum34==1.1.6
functools32==3.2.3.post2
idna==2.5
ipaddress==1.0.19
jsonschema==2.6.0
pyyaml==3.12
requests==2.18.4
six==1.11.0
texttable==0.9.1
urllib3==1.22
websocket-client==0.47.0

